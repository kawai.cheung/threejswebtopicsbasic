import * as THREE from "https://cdn.skypack.dev/pin/three@v0.132.2-1edwuDlviJO0abBvWgKd/mode=imports/optimized/three.js";

//canvas selecteren
const canvas = document.getElementById("canvas");

//scene
const scene = new THREE.Scene();

//mesh object bestaat uit geometry en material
const geometry = new THREE.BoxGeometry(1, 1, 1);
const material = new THREE.MeshBasicMaterial({ color: 0x4188fa });
const mesh = new THREE.Mesh(geometry, material);
scene.add(mesh);

// Camera
const camera = new THREE.PerspectiveCamera(
  75,
  window.innerWidth / window.innerHeight
);
camera.position.z = 3;
scene.add(camera);

// Renderer
const renderer = new THREE.WebGLRenderer({ canvas });
renderer.setSize(window.innerWidth, window.innerHeight);

const clock = new THREE.Clock();

const animate = () => {
  renderer.render(scene, camera);

  //rotate animation
  mesh.rotation.y = clock.getElapsedTime();
  mesh.rotation.x = clock.getElapsedTime();

  // animate functie oproepen volgende frame
  window.requestAnimationFrame(animate);
};

animate();

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// const rotate = document.querySelector(".rotatie");
// const real = document.querySelector(".realistic");
// rotate.addEventListener("click", () => {
//   state.rotate = !state.rotate;
// });
// real.addEventListener("click", () => {
//   state.realistic = !state.realistic;
// });

// const rotation = () => {
//   mesh.rotation.x = clock.getElapsedTime();
//   mesh.rotation.z = clock.getElapsedTime();
// };

// const realistic = () => {
//   const meshPlane = new THREE.Mesh(
//     new THREE.PlaneGeometry(80, 40),
//     new THREE.MeshStandardMaterial()
//   );
//   meshPlane.rotation.x = -Math.PI / 2;
//   meshPlane.position.y = -1;

//   mesh.material = new THREE.MeshStandardMaterial();

//   const ambientLight = new THREE.AmbientLight(0xffffff, 0.1);
//   const directionalLight = new THREE.DirectionalLight(0x00fffc, 0.7);
//   const pointLight = new THREE.PointLight(0xff9000, 0.5);
//   directionalLight.position.set(1, 1, 1);
//   scene.add(directionalLight, meshPlane, pointLight, ambientLight);
//   state.realistic = false;

//   //shadow
//   // renderer.shadowMap.enabled = true;
//   // meshPlane.receiveShadow = true;
//   // mesh.castShadowµ = true;
//   // directionalLight.castShadow = true;
//   real.setAttribute("disabled", true);
// };
