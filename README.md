# ThreeJsWebTopicsBasic

## How to run

- open command line
- git clone https://gitlab.com/kawai.cheung/threejswebtopicsbasic.git
- open project in visual studio code
- install live server in visual studio code if not already done (https://marketplace.visualstudio.com/items?itemName=ritwickdey.LiveServer)
- run live server
- to view other Three.js example uncomment line 12 and 14 and put line 11 and 13 in comments
