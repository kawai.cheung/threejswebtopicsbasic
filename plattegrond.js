import * as THREE from "https://cdn.skypack.dev/pin/three@v0.132.2-1edwuDlviJO0abBvWgKd/mode=imports/optimized/three.js";
import { OrbitControls } from "https://unpkg.com/three@0.127.0/examples/jsm/controls/OrbitControls.js";

/**
 * Base
 */
//canvas
const canvas = document.getElementById("plattegrond");

// Sizes
let sizes = {
  width: window.innerWidth,
  height: window.innerHeight,
};

// Scene
const scene = new THREE.Scene();

//group
const group = new THREE.Group();

//lokaalnr objecten bijhouden0xa8a8a8
let lokaalnr = {};
//lokaal objecten bijhouden 0x505050
let lokalen = {};

//creat room function
const createObject = (x, y, z, w, h, d, color, naam) => {
  const mesh = new THREE.Mesh(
    new THREE.BoxGeometry(w, h, d),
    new THREE.MeshStandardMaterial({ color: color })
  );

  mesh.position.set(x, y, z);
  mesh.castShadow = true;
  group.add(mesh);
  lokalen[naam] = mesh;
  return mesh;
};

//create bushes function
const createBush = (x, y, z) => {
  const geometry = new THREE.SphereGeometry(0.15, 4, 4);
  const material = new THREE.MeshStandardMaterial({ color: 0x4cbb17 });
  const sphere = new THREE.Mesh(geometry, material);
  sphere.position.set(x, y, z);
  sphere.castShadow = true;
  sphere.rotation.y = -Math.PI / 4;
  group.add(sphere);
};

for (let i = -2.4; i <= -1.14; i = i + 0.21) {
  createBush(i, 0.6, 2.425);
}

for (let i = 0.1; i <= 5.04; i = i + 0.21) {
  createBush(i, 0.6, 2.425);
}

const createTree = (x, y, z) => {
  const geometry = new THREE.SphereGeometry(0.2, 16, 4);
  const material = new THREE.MeshStandardMaterial({ color: 0x0b6623 });
  const sphere = new THREE.Mesh(geometry, material);
  sphere.position.set(x, y, z);
  sphere.castShadow = true;
  sphere.rotation.y = -Math.PI / 4;
  group.add(sphere);
};

//white strokes
const whiteStrokes = (x, y, z) => {
  const geometry = new THREE.PlaneGeometry(0.1, 0.5, 32);
  const material = new THREE.MeshBasicMaterial({ color: 0xffffff });
  const plane = new THREE.Mesh(geometry, material);
  plane.position.set(x, y, z);
  plane.rotation.x = -Math.PI / 2;
  plane.rotation.z = Math.PI / 2;
  group.add(plane);
};

for (let i = -4.5; i <= 4.5; i = i + 1) {
  whiteStrokes(i, 0.41, 3.5);
}

//bomen
createObject(0.5, 0.7, 2, 0.1, 0.4, 0.1, 0x53350a, "boom1");
createObject(2.5, 0.7, 2, 0.1, 0.4, 0.1, 0x53350a, "boom2");
createObject(4.5, 0.7, 2, 0.1, 0.4, 0.1, 0x53350a, "boom2");
createTree(0.5, 1, 2);
createTree(2.5, 1, 2);
createTree(4.5, 1, 2);
// floor
const block1 = createObject(0, 0, 0, 10, 1, 5, 0x505050, "D-gang");
const weg1 = createObject(0, 0, 2.75, 10, 1, 0.5, 0x505050, "voetpad");
weg1.castShadow = false;
weg1.receiveShadow = true;
const weg2 = createObject(0, -0.05, 3.5, 10, 0.9, 1, 0x303030, "weg");
weg2.castShadow = false;
weg2.receiveShadow = true;
const block2 = createObject(-4.5, 0.76, 0, 1, 0.5, 5, 0xa8a8a8, "block1");
const block3 = createObject(-3, 0.76, -1.25, 1, 0.5, 2.5, 0xa8a8a8, "block2");
const block4 = createObject(-2.25, 0.76, 1, 2.5, 0.5, 1, 0xa8a8a8, "block3");
const block5 = createObject(-3, 0.76, 2.25, 1, 0.5, 0.5, 0xa8a8a8, "block4");
const block6 = createObject(-1.75, 0.76, -0.5, 1.5, 0.5, 1, 0xa8a8a8, "block5");
const block7 = createObject(0.625, 0.76, -0.5, 1.25, 0.5, 1, 0xa8a8a8, "D27");
const vloer1 = createObject(
  1.125,
  0.46,
  0.25,
  2.25,
  0.1,
  0.5,
  0xa8a8a8,
  "D-gang1"
);
vloer1.material.color = new THREE.Color(0x505050);
vloer1.castShadow = false;
vloer1.receiveShadow = true;
const vloer2 = createObject(
  3.375,
  0.46,
  0.25,
  2.25,
  0.1,
  0.5,
  0xa8a8a8,
  "D-gang2"
);
vloer2.material.color = new THREE.Color(0x505050);
vloer2.castShadow = false;
vloer2.receiveShadow = true;
const block71 = createObject(1.875, 0.76, -0.5, 1.2, 0.5, 1, 0xa8a8a8, "D29");
const block8 = createObject(2.985, 0.76, -0.5, 0.975, 0.5, 1, 0xa8a8a8, "D30");
const block9 = createObject(4.25, 0.76, -0.5, 1.5, 0.5, 1, 0xa8a8a8, "D31");
const block10 = createObject(
  4.75,
  0.76,
  0.515,
  0.5,
  0.5,
  0.975,
  0xa8a8a8,
  "D32"
);
const block11 = createObject(3.25, 0.76, 1, 1.5, 0.5, 1, 0xa8a8a8, "D34");
const block12 = createObject(2, 0.76, 1, 0.95, 0.5, 1, 0xa8a8a8, "D35");
const block13 = createObject(
  0.75,
  0.76,
  1.25,
  0.5,
  0.5,
  0.5,
  0xa8a8a8,
  "block13"
);
const block14 = createObject(0.25, 0.76, 1, 0.5, 0.5, 1, 0xa8a8a8, "block14");
const block15 = createObject(1.25, 0.76, 1, 0.5, 0.5, 1, 0xa8a8a8, "block15");
const deur = createObject(-0.5, 0.76, 1.45, 1, 0.5, 0.1, 0xf6feff, "deur");
const muur1 = createObject(-3.75, 0.76, 2.45, 0.5, 0.5, 0.1, 0xa8a8a8, "muur1");
const muur2 = createObject(0.05, 0.76, -1.75, 0.1, 0.5, 1.5, 0xa8a8a8, "muur2");
const muur3 = createObject(
  -1.05,
  0.76,
  -1.75,
  0.1,
  0.5,
  1.5,
  0xa8a8a8,
  "muur3"
);
const muur4 = createObject(4.5, 0.76, 1.45, 1, 0.5, 0.1, 0xa8a8a8, "muur4");
const muur5 = createObject(4.95, 0.76, 1.25, 0.1, 0.5, 0.5, 0xa8a8a8, "muur5");
const deur2 = createObject(-2.55, 0.76, 1.75, 0.1, 0.5, 0.5, 0xf6feff, "deur2");
const deur3 = createObject(
  -3.75,
  0.76,
  -2.45,
  0.5,
  0.5,
  0.1,
  0xa8a8a8,
  "deur3"
);
deur2.material.transparent = true;
deur2.material.opacity = 0.3;
deur.material.transparent = true;
deur.material.opacity = 0.3;
deur3.material.transparent = true;
deur3.material.opacity = 0.3;

block1.castShadow = false;
block1.receiveShadow = true;

//add group to scene
scene.add(group);

//car
const Wheel = () => {
  const zoom = 1.25;
  const wheel = new THREE.Mesh(
    new THREE.BoxBufferGeometry(12 * zoom, 33 * zoom, 12 * zoom),
    new THREE.MeshLambertMaterial({ color: 0x333333, flatShading: true })
  );
  wheel.position.z = 6 * zoom;
  return wheel;
};

const addCar = (addToScene = true) => {
  const zoom = 1.25;
  const carFrontTexture = new THREE.Texture(40, 80, [
    { x: 0, y: 10, w: 30, h: 60 },
  ]);
  const carBackTexture = new THREE.Texture(40, 80, [
    { x: 10, y: 10, w: 30, h: 60 },
  ]);
  const carRightSideTexture = new THREE.Texture(110, 40, [
    { x: 10, y: 0, w: 50, h: 30 },
    { x: 70, y: 0, w: 30, h: 30 },
  ]);
  const carLeftSideTexture = new THREE.Texture(110, 40, [
    { x: 10, y: 10, w: 50, h: 30 },
    { x: 70, y: 10, w: 30, h: 30 },
  ]);

  const car = new THREE.Group();
  const color = "#" + Math.floor(Math.random() * 16777215).toString(16);

  const main = new THREE.Mesh(
    new THREE.BoxBufferGeometry(60 * zoom, 25 * zoom, 15 * zoom),
    new THREE.MeshPhongMaterial({ color, flatShading: true })
  );
  main.position.z = 12 * zoom;
  main.castShadow = true;
  main.receiveShadow = true;
  car.add(main);

  const cabin = new THREE.Mesh(
    new THREE.BoxBufferGeometry(33 * zoom, 24 * zoom, 12 * zoom),
    [
      new THREE.MeshPhongMaterial({
        color: 0xcccccc,
        flatShading: true,
        map: carBackTexture,
      }),
      new THREE.MeshPhongMaterial({
        color: 0xcccccc,
        flatShading: true,
        map: carFrontTexture,
      }),
      new THREE.MeshPhongMaterial({
        color: 0xcccccc,
        flatShading: true,
        map: carRightSideTexture,
      }),
      new THREE.MeshPhongMaterial({
        color: 0xcccccc,
        flatShading: true,
        map: carLeftSideTexture,
      }),
      new THREE.MeshPhongMaterial({ color: 0xcccccc, flatShading: true }), // top
      new THREE.MeshPhongMaterial({ color: 0xcccccc, flatShading: true }), // bottom
    ]
  );
  cabin.position.x = 6 * zoom;
  cabin.position.z = 25.5 * zoom;
  cabin.castShadow = true;
  cabin.receiveShadow = true;
  car.add(cabin);

  const frontWheel = Wheel();
  frontWheel.position.x = -18 * zoom;
  car.add(frontWheel);

  const backWheel = Wheel();
  backWheel.position.x = 18 * zoom;
  car.add(backWheel);

  car.castShadow = true;
  car.receiveShadow = false;

  if (addToScene) scene.add(car);

  const scale = 0.008;
  car.scale.set(scale, scale, scale);
  car.position.set(3, 0.4, 3.2);
  car.rotation.x = -Math.PI / 2;
  group.add(car);

  return car;
};

const car = addCar();
const car2 = addCar();
car2.position.set(-3, 0.4, 3.8);
car2.rotation.z = -Math.PI;

//ambientlightning
const light = new THREE.AmbientLight(0x666661);
scene.add(light);

//directional light
const directionalLight = new THREE.DirectionalLight(0xe8e8e8, 0.8);
scene.add(directionalLight);
directionalLight.position.y = 5;
directionalLight.position.z = 3;
directionalLight.position.x = -3;
directionalLight.castShadow = true;

// Camera
const camera = new THREE.PerspectiveCamera(75, sizes.width / sizes.height);
camera.position.z = 6;
camera.position.y = 2.5;
camera.position.x = -3;
scene.add(camera);

const controls = new OrbitControls(camera, canvas);
controls.enableDamping = true;

//D3 Text
const loader = new THREE.FontLoader();

let font = null;

const createText = function (tekst, x, y, z) {
  const geometry = new THREE.TextGeometry(tekst, {
    font: font,
    size: 0.25,
    height: 0.075,
  });
  const textMaterial = new THREE.MeshStandardMaterial({ color: 0xa8a8a8 });
  const text = new THREE.Mesh(geometry, textMaterial);
  text.position.set(x, y, z);
  lokaalnr[tekst] = text;
  group.add(text);
};

loader.load(
  "https://cdn.rawgit.com/mrdoob/three.js/master/examples/fonts/helvetiker_regular.typeface.json",
  function (json) {
    font = json;
    createText("D27", 0.3, 1.025, -0.5);
    createText("D29", 1.5, 1.025, -0.5);
    createText("D30", 2.7, 1.025, -0.5);
    createText("D31", 3.85, 1.025, -0.5);
    createText("D32", 4.75, 1.025, 0.2);
    createText("D34", 3, 1.025, 0.95);
    createText("D35", 1.7, 1.025, 0.95);
    lokaalnr.D32.rotation.y = -Math.PI / 2;
  }
);

console.log(lokaalnr);
console.log(lokalen);

// Renderer
const renderer = new THREE.WebGLRenderer({
  canvas,
});

renderer.setSize(sizes.width, sizes.height);
renderer.setPixelRatio(Math.min(window.devicePixelRatio, 2));
renderer.shadowMap.enabled = true;
renderer.shadowMap.type = THREE.PCFSoftShadowMap;

directionalLight.shadow.mapSize.width = 1024;
directionalLight.shadow.mapSize.height = 1024;
directionalLight.shadow.camera.near = 0.5; // default
directionalLight.shadow.camera.far = 500; // default

let speed = 0.025;
let speed2 = 0.05;
// Animate
const clock = new THREE.Clock();

const tick = () => {
  const elapsedTime = clock.getElapsedTime();

  controls.update();
  // Render
  renderer.render(scene, camera);

  if (car && car.visible) {
    car.position.x -= speed;

    if (car.position.x < -4) {
      car.visible = false;
      setTimeout(() => {
        car.position.x = 4;
        car.visible = true;
        speed = Math.random() * (0.25 - 0.025) + 0.025;
        car.children[0].material.color = new THREE.Color(
          "#" + Math.floor(Math.random() * 16777215).toString(16)
        );
      }, 1000);
    }
  }
  if (car2 && car2.visible) {
    car2.position.x += speed2;

    if (car2.position.x > 4) {
      car2.visible = false;
      setTimeout(() => {
        car2.position.x = -4;
        car2.visible = true;
        speed2 = Math.random() * (0.25 - 0.025) + 0.025;
        car2.children[0].material.color = new THREE.Color(
          "#" + Math.floor(Math.random() * 16777215).toString(16)
        );
      }, 1000);
    }
  }

  // Call tick again on the next frame
  window.requestAnimationFrame(tick);
};

tick();
